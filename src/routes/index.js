import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import Accounts from '../views/Accounts.vue'
import Sales from '../views/Sales.vue'
import SingleAccount from '../views/SingleAccount.vue'
import SingleOrder from '../views/SingleOrder.vue'
import SingleProduct from '../views/SingleProduct.vue'
import Product from '../views/Product.vue'
import Progress from '../views/Progress.vue'
import Reports from '../views/Reports.vue'
import Contact from '../views/Contact.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
    },
    {
        path: "/",
        name: "login",
        component: () => import("../views/Auth/login.vue")
    },
    {
        path: "/register",
        name: "register",
        component: () => import("../views/Auth/register.vue")
    },
    {
        path: '/accounts',
        name: 'Accounts',
        component: Accounts,
    },
    {
        path: '/accounts/:id/edit',
        name: 'Single Account',
        component: SingleAccount,
    },
    {
        path: '/orders',
        name: 'Orders',
        component: Sales,
    },
    {
        path: '/orders/edit/:id',
        name: 'Single Order',
        component: SingleOrder,
    },
    {
        path: '/progress',
        name: 'Progress',
        component: Progress,
    },
    {
        path: '/products',
        name: 'Product',
        component: Product,
    },
    {
        path: '/products/edit/:id',
        name: 'Single Product',
        component: SingleProduct,
    },
    {
        path: '/reports',
        name: 'Reports',
        component: Reports,
    },
    {
        path: '/contacts',
        name: 'Contact',
        component: Contact,
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem("jwt") == null) {
            next({
                path: "/"
            });
        } else {
            next();
        }
    } else {
        next();
    }
});
export default router
