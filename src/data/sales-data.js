export const saleChartData = {
    type: "bar",
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [
            {
                data: [116, 86, 93, 42, 78, 82, 27, 79, 143, 103, 96, 178],
                backgroundColor: "rgba(54,73,93,.5)",
                borderColor: "#36495d",
            }
        ]
    },
    options: {
        responsive: true,
        lineTension: 1,
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                        padding: 25
                    }
                }
            ]
        }
    }
};

export default saleChartData;
